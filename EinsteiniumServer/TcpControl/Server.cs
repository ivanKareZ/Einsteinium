﻿using EinsteiniumCommon;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace EinsteiniumServer
{
    public class Server
    {
        private int maxClientCount;
        public int MaxClientCount
        {
            get { return maxClientCount; }
        }

        private int port;
        public int Port
        {
            get { return port; }
        }

        private string ipAddress;
        public string IpAddress
        {
            get { return ipAddress; }
        }

        private int currentClientCount;
        public int CurrentClientCount
        {
            get { return currentClientCount; }
        }

        public delegate void OnClientConnectedDelegate(TcpClient client);
        public event OnClientConnectedDelegate OnClientConnected;

        public delegate void OnClientDisconnectedDelegate();
        public event OnClientDisconnectedDelegate OnClientDisconnected;

        public delegate void OnRequestRecievedDelegate(Request request);
        public event OnRequestRecievedDelegate OnRequestRecieved;

        public delegate void OnResponseSendedDelegate(Response response);
        public event OnResponseSendedDelegate OnResponseSended;

        Thread serverThread;
        Dictionary<int, Thread> clientThreads;
        Dictionary<int, Protocol> protocols;
        ResponderListBuilder respondeListBuilder;

        public Server(int maxClientCount, string ipAddress, int port, ResponderListBuilder builder)
        {
            clientThreads = new Dictionary<int, Thread>();
            protocols = new Dictionary<int, Protocol>();
            this.maxClientCount = maxClientCount;
            this.ipAddress = ipAddress;
            this.port = port;
            this.respondeListBuilder = builder;
        }

        public void Start()
        {
            serverThread = new Thread(Loop);
            serverThread.Priority = ThreadPriority.AboveNormal;
            serverThread.Start();
        }

        void Loop()
        {
            TcpListener listener = new TcpListener(IPAddress.Parse(ipAddress),port);
            listener.Start();
            int pCounter = 0;
            do
            {
                TcpClient client = listener.AcceptTcpClient();
                if (currentClientCount < MaxClientCount)
                {
                    Protocol protocol = new Protocol(client, respondeListBuilder.GetResponderList(), this, pCounter);
                    Thread clientThread = new Thread(protocol.BeginCommunication);
                    clientThread.Start();
                    OnClientConnected?.Invoke(client);
                    protocols.Add(pCounter, protocol);
                    clientThreads.Add(pCounter, clientThread);
                    currentClientCount++;
                    pCounter++;
                }
                else
                {
                    client.Close();
                }

            } while (true);
        }

        public void RemoveProtocol(int id)
        {
            protocols.Remove(id);
            clientThreads.Remove(id);
            Debug.WriteLine("Protocol "+ id +" removed.");
            currentClientCount--;
            OnClientDisconnected?.Invoke();
        }

        public void TriggerRequestRecieved(Request request)
        {
            OnRequestRecieved?.Invoke(request);
        }

        public void TriggerResponseSended(Response response)
        {
            OnResponseSended?.Invoke(response);
        }
    }
}
