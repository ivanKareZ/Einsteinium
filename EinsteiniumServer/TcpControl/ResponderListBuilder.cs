﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EinsteiniumServer
{
    public interface ResponderListBuilder
    {
        List<RequestResponder> GetResponderList();
    }
}
