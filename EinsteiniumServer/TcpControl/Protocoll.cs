﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using EinsteiniumCommon;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Diagnostics;
using System.IO;

namespace EinsteiniumServer
{
    class Protocol
    {
        TcpClient client;
        List<RequestResponder> responders;
        Server server;
        int id;

        public Protocol(TcpClient client, List<RequestResponder> responders, Server server, int id)
        {
            this.client = client;
            this.responders = responders;
            this.server = server;
            this.id = id;
        }

        public void BeginCommunication()
        {
            NetworkStream clientStream = client.GetStream();
            do
            {
                try
                {
                    SendResponse(clientStream);
                }
                catch(Exception)
                {
                    Debug.WriteLine("Loosing client!");
                    break;
                }
            } while (true);
            server.RemoveProtocol(id);
        }

        void SendResponse(NetworkStream clientStream)
        {
            Response response;
            try
            {
                Request request = FetchRequestFromClientStream(clientStream);
                server.TriggerRequestRecieved(request);
                RequestResponder responder = GetResponderForRequest(request);
                response = responder.ResponseToRequest(request);
            }
            catch (Exception e)
            {
                object payload = null;
                Error error = new Error(e);
                response = new Response(payload, error);
            }
            SendResponseToClient(clientStream, response);
            server.TriggerResponseSended(response);
        }

        RequestResponder GetResponderForRequest(Request request)
        {
            string commandName = request.Message;
            foreach (RequestResponder responder in responders)
            {
                if (responder.CommandName == commandName || responder.CommandName == "*")
                    return responder;
            }
            throw new Exception(String.Format("Responder not found for '{}'!",commandName));
        }

        Request FetchRequestFromClientStream(NetworkStream clientStream)
        {
            IFormatter formatter = new BinaryFormatter();
            Request request = (Request)formatter.Deserialize(clientStream);
            return request;
        }

        void SendResponseToClient(NetworkStream stream, Response response)
        {
            IFormatter formatter = new BinaryFormatter();
            formatter.Serialize(stream, response);
            stream.Flush();
        }
    }
}
