﻿using System;
using System.Collections.Generic;
using System.Text;
using EinsteiniumCommon;

namespace EinsteiniumServer
{
    public abstract class RequestResponder
    {
        private string commandName;
        public string CommandName
        {
            get { return commandName; }
        }

        public RequestResponder(string commandName)
        {
            this.commandName = commandName;
        }

        public abstract Response ResponseToRequest(Request request);
    }
}
