﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EinsteiniumCommon
{
    [Serializable]
    public class Request
    {
        private string message;
        public string Message
        {
            get { return message; }
            set { message = value; }
        }

        protected object payload;
        public object Payload
        {
            get { return payload; }
        }

        public Request(string message, object payload)
        {
            this.message = message;
            this.payload = payload;
        }
    }
}
