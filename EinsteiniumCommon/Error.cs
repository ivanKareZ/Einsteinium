﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EinsteiniumCommon
{
    /// <summary>
    /// Exception wrapper amit lehet szerializálni.
    /// </summary>
    [Serializable]
    public class Error
    {
        private string message;
        public string Message
        {
            get { return message; }
        }

        private Error innerError;
        public Error InnerError
        {
            get { return innerError; }
        }

        private string source;
        public string Source
        {
            get { return source; }
        }

        private string stackTrace;
        public string StackTrace
        {
            get { return stackTrace; }
        }

        public Error(string message, Error innerError)
        {
            this.message = message;
            this.innerError = innerError;
        }

        public Error(string message)
        {
            this.message = message;
            this.innerError = null;
        }

        public Error(Exception exception)
        {
            this.message = exception.Message;
            this.stackTrace = exception.StackTrace;
            this.source = exception.Source;
            if(exception.InnerException != null)
                this.innerError = new Error(exception.InnerException);
        }

    }
}
