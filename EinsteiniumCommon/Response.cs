﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EinsteiniumCommon
{
    [Serializable]
    public class Response
    {
        private Error exception;
        public Error Exception
        {
            get { return exception; }
        }

        public bool HasException
        {
            get { return exception != null; }
        }

        protected object payload;
        public object Payload
        {
            get { return payload; }
        }

        public Response(object payload, Error exception)
        {
            this.payload = payload;
            this.exception = exception;
        }

        public Response(object payload)
            :this(payload, null)
        { }
    }
}
