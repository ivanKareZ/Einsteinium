﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using EinsteiniumCommon;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace EinsteiniumClient
{
    public class Client
    {
        TcpClient client;
        NetworkStream stream;
        string ip;
        int port;

        public Client(string ip, int port)
        {
            this.ip = ip;
            this.port = port;
            client = new TcpClient();           
        }

        public void Connect()
        {
            client.Connect(IPAddress.Parse(ip), port);
            stream = client.GetStream();
        }

        public Response SendRequest(string message, object payload)
        {
            Request request = new Request(message, payload);
            SerializeRequest(request);
            Response response = FetchBackResponse();
            return response;
        }

        void SerializeRequest(Request request)
        {
            IFormatter formater = new BinaryFormatter();
            formater.Serialize(stream, request);
            stream.Flush();
        }

        Response FetchBackResponse()
        {
            IFormatter formater = new BinaryFormatter();
            Response response = (Response)formater.Deserialize(stream);
            return response;
        }

        public void Disconnect()
        {
            client.Close();
        }
    }
}
